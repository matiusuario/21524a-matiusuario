const fetchPosts = async () => {
    try {
		let response = await fetch("/posts");
		let posts = await response.json();
		return posts;
	} catch (error) {
		console.error("Error durante la consulta de las imagenes: ", error.message);
		return -1;
	}
}

const postearPost = async (datos) => {
    try {
        const response = await fetch("/post", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(datos)
        });
        const result = await response.json();
        return result;
    } catch (error) {
        console.error("Error durante la publicación", error.message);
        return -1;
    }
}

const borrarPost = async (id) => {
    try {
        let response = await fetch("/delete/"+id, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
            }
        });
        const result = await response.json();
        return result;
    } catch (error) {
        console.error(error.message);
    }
}

const editarPost = async (post) => {
    try {
        const { id } = post;
        let response = await fetch("/edit/"+id, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(post)
        });
        const result = await response.json();
        return result;
    } catch (error) {
        console.error(error.message);
    }
}

const fetchPost = async (id) => {
    try {
        let response = await fetch("/edit/"+id);
        let post = await response.json();
        return post;
    } catch (error) {
        console.error("Error consultando la publicación: ", error.message);
        return -1;
    }
}

export { fetchPosts, postearPost, borrarPost, editarPost, fetchPost };
