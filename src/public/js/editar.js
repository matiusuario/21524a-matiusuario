import { fetchPost, editarPost } from "./funciones.js";
const cancelar = document.querySelector("#cancelar");
const form = document.querySelector("#editarForm");

cancelar.addEventListener("click", () => history.back());

form.addEventListener("submit", async (event) => {
    
    event.preventDefault();
    const idPost = document.querySelector("#idPost").value;
    const titulo = document.querySelector("#titulo").value;
    const url = document.querySelector("#url").value;
    const fecha = document.querySelector("#fecha").value;
    const contenido = document.querySelector("#contenido").value;
    const post = {
        id: idPost,
        titulo: titulo,
        contenido: contenido,
        url_imagen: url,
        fecha_creacion: fecha
    }
    const resp = await editarPost(post);
    alert(resp.msg);
    window.location.href = "/";
});
