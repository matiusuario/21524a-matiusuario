import { Sequelize, DataTypes } from "sequelize";

const sequelize = new Sequelize(
    process.env.DBNAME,
    process.env.DBUSERNAME,
    process.env.DBPASS,
    {
        host: process.env.HOST,
        dialect: process.env.DIALECT
    }
);

const conectarDb = async () => {
    try {
        await sequelize.authenticate();
        console.log("Conexión con DDBB establecida con éxito.");
        await sequelize.sync();
        console.log("Sincronización con DDBB exitosa.");
      } catch (error) {
        console.error("Error al conectarse o sincronizar la base de datos:", error);
      }
}

export { DataTypes, Sequelize, sequelize, conectarDb };
