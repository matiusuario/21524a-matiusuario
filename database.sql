-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 29, 2023 at 05:01 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ap_integrador`
--

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `contenido` text NOT NULL,
  `url_imagen` varchar(255) NOT NULL,
  `fecha_creacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `titulo`, `contenido`, `url_imagen`, `fecha_creacion`) VALUES
(1, 'El software libre y la educación', 'La libertad del software asume un rol de fundamental importancia en el ámbito educativo. Las instituciones educativas de todos los niveles deben utilizar y enseñar exclusivamente software libre porque es el único que les permite cumplir con sus misiones fundamentales: difundir el conocimiento y enseñar a los estudiantes a ser buenos miembros de su comunidad. El código fuente y los métodos del software libre son parte del conocimiento humano. Al contrario, el software privativo es conocimiento secreto y restringido y, por lo tanto, se opone a la misión de las instituciones educativas. El software libre favorece la enseñanza, mientras el software privativo la prohíbe.\r\n\r\nEl software libre no es simplemente un asunto técnico, es un asunto ético, social y político. Es una cuestión de derechos humanos que los usuarios de software deben tener. La libertad y la cooperación son valores esenciales del software libre. El sistema GNU pone en práctica estos valores y el principio del compartir, pues compartir es bueno y útil para el progreso de la humanidad.\r\n[Texto de gnu.org]', 'https://i.ytimg.com/vi/gtnT661uE64/maxresdefault.jpg', '2023-09-13 21:04:31'),
(2, 'Los beneficios del ejercicio cardiovascular', 'Andar, correr, nadar o montar en bicicleta son típicos ejercicios aeróbicos. El término aeróbico significa \"con oxígeno\", lo que significa que la respiración controla la cantidad de oxígeno que puede llegar a los músculos para ayudar a que quemen combustible y se muevan. Sin embargo, la clave no está tanto en la actividad en sí como en la intensidad al realizarlas . El ejercicio aeróbico es aquel que se desarrolla a intensidad media o baja durante periodos de tiempo largos con el objeto de conseguir mayor resistencia.\r\n\r\nEn la práctica de este tipo de ejercicios la energía se obtiene quemando hidratos y grasas para lo que es necesario oxígeno. Además, hacen que el corazón bombee la sangre más rápidamente y con mayor fuerza. Al bombear más rápido, el requerimiento de oxígeno se incrementa y se acelera la respiración. Con esto, también se fortalece el corazón y se favorece la capacidad pulmonar. Por eso, la Organización Mundial de la Salud recomienda dedicar al menos 150 minutos a la práctica de actividad física aeróbica, de intensidad moderada, o bien 75 minutos de actividad física aeróbica vigorosa cada semana, o bien una combinación equivalente de actividades moderadas y vigorosas\r\n[Texto de abc.es]', 'https://www.axahealthkeeper.com/wp-content/uploads/2019/11/10-ejercicios-para-mejorar-tu-resistencia-cardiovascular.jpg', '2023-09-13 21:06:59'),
(3, 'Tomá mate y avivate', 'El mate es mucho más que una simple infusión tonificante: es una tradición irrenunciable, un ritual social y un estilo de vida. Cualquier argentino, uruguayo o paraguayo que uno conozca puede decir maravillas del mate, y por poco que se haya viajado a esos países ya se habrá percibido.\n\nPero lo primero que se nos aclararía es que lo correcto es llamar «yerba mate» o «hierba mate» –y no «mate»– a la planta Ilex paraguaiensis, de la que se obtiene esta infusión.\n\nSe trata de un arbusto de hoja perenne que puede alcanzar los 15 metros de alto, con las hojas oval-lanceoladas, dentadas en sus márgenes, flores blancas y frutos esféricos de color rojo.\n\nCrece en ambientes subtropicales cálidos y húmedos de Paraguay, sur de Brasil (Matogrosso), norte de Argentina (Misiones y Corrientes) y Uruguay.\n\nLa infusión tiene un sabor algo amargo y un claro efecto estimulante. No en vano se dice en Argentina: toma mate y avivate, sin el acento, por supuesto.\n\nLa costumbre es tomarlo en compañía, pasándose el mate de mano en mano, como dios manda.', 'https://i.pinimg.com/736x/60/69/52/606952f7f9e3f7e1b3f3ca0991f00e7d--colon-cancer-cancer-cells.jpg', '2023-09-24 00:00:00'),
(5, '10 temas interesantes al azar para hablar', 'Puedes usar una rueda giratoria para elegir un tema de discurso extraño y aleatorio, ya que es divertido o un tema interesante para hablar\n\n    Trece es un número de la suerte.\n    10 mejores maneras de hacer que tus hijos te dejen en paz.\n    10 maneras de molestar a tus padres.\n    Problemas de chicas calientes.\n    Los chicos chismean más que las chicas.\n    Culpa a tus gatos de tus problemas.\n    No tome la vida demasiado en serio.\n    Si los hombres tuvieran un ciclo menstrual.\n    Controla tu risa en los momentos serios.\n    El juego de Monopoly es un deporte mental.\n', 'https://ahaslides.com/wp-content/uploads/2022/09/11879376_40-1024x1024.webp', '2023-09-28 00:00:00'),
(9, 'Lorem ipsum dolor sit amet', '\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consequat, tellus et malesuada bibendum, tortor nisl iaculis lorem, ac sodales risus erat sit amet sapien. In hac habitasse platea dictumst. Fusce ullamcorper et nisi ac tincidunt. Nullam dignissim sodales interdum. Cras scelerisque augue vitae nisl molestie, sit amet suscipit felis tristique. Nunc quis cursus magna. Nulla efficitur justo ut libero posuere, ut efficitur urna rutrum. Praesent risus ex, dictum eu faucibus eget, dignissim vitae massa. Praesent malesuada nunc quis scelerisque tristique. Curabitur eget justo interdum velit ornare vulputate. Nam a eleifend mauris. Suspendisse mattis urna id turpis eleifend tempus in et arcu.\n\nVivamus lobortis efficitur nulla at faucibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed felis ex, facilisis sit amet convallis vel, dapibus et nibh. Fusce aliquam dolor nibh, at iaculis mi hendrerit ut. Nulla semper magna eu lectus tincidunt auctor. Morbi leo tellus, placerat eget volutpat id, congue ultrices quam. Vivamus id efficitur nunc. Nam odio velit, hendrerit in blandit non, rhoncus vitae enim. Mauris libero quam, lacinia a finibus quis, auctor quis nisi. In hac habitasse platea dictumst. Interdum et malesuada fames ac ante ipsum primis in faucibus. Phasellus sed viverra neque. Curabitur aliquam massa id elit rhoncus porta. Nam fringilla laoreet ex, vel malesuada urna faucibus in. Morbi rhoncus velit non vehicula dapibus. Aliquam dapibus nisl sed sagittis tincidunt. ', 'https://cdn8.openculture.com/wp-content/uploads/2015/03/22140851/lorem-2-1024x443.png', '2023-09-25 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
