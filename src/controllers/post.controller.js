import { Post } from "../models/post.js";

const ctr = {};

ctr.newPost = async (req, res) => {

    try {
        const post = await Post.create(req.body);
        await post.save();
        res.send({msg: "Publicación guardada con éxito", post});

    } catch (error) {
        console.error(error.message);
        return res.status(500).json({ msg: "Error al guardar la publicación" });
    }
}

ctr.getPost = async (req, res) => {

    try {
        const { id } = req.params;
        const post = await Post.findByPk(id);
        return post;
    } catch (error) {
        console.error("Error: ",error);
        return res.status(500).json({ msg: "Error al obtener la publicación" });
    }
}

ctr.getPosts = async (req, res) => {

    try {
        const posts = await Post.findAll();
        return res.json(posts);

    } catch (error) {
        console.error(error.message);
        return res.status(500).json({ msg: "Error al obtener las publicaciones" });
    }
}

ctr.updatePost = async (req, res) => {

    try {
        const { id } = req.params;
        const post = await Post.findByPk(id);
        post.set(req.body);
        await post.save();
        return res.json({ msg: "Publicación actualizada con éxito"});

    } catch (error) {
        console.error(error.message);
        return res.status(500).json({ msg: "Error al actualizar la publicación" });
    }
}

ctr.deletePost = async (req, res) => {

    try {
        const { id } = req.params;
        const post = await Post.findByPk(id);

        await post.destroy({
            where: {
                id
            }
        });
        return res.json({ msg: "Publicación eliminada con éxito"});

    } catch (error) {
        console.error(error.message);
        return res.status(500).json({ msg: "Error al eliminar la publicación" });
    }
}

export { ctr };
