# Proyecto integrador Argentina Programa 4.0

## EPICA - Tramo II - Lenguajes de programación I - Comisión 21524A

Un foro personalizado para crear contenido multitemático.

- Clonar
- Instalar dependencias con npm i
- (Se puede utilizar xampp)
- Agregar base de datos
- Agregar archivo .env
- Ejecutar
- Enjoy!

## License

Este proyecto está licenciado bajo la GNU Affero General Public License v3.0. [AGPL-3.0](LICENSE)
