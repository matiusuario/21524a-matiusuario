import { Router } from "express";
import { ctr } from "../controllers/post.controller.js";

const router = Router();

//Genera vista home
router.get("/", (req, res) => {
    res.render("home");
});
//Genera vista publicar
router.get("/post", (req, res) => {
    res.render("publicar");
});
//Genera vista editar Y llama controlador para obtener la publicacion
router.get("/edit/:id", async (req, res) => {
    const ogPost = await ctr.getPost(req, res);
    res.render("editar", {post: ogPost});
});

//Delega respuestas a APIs en controladores
router.put("/edit/:id", ctr.updatePost);

router.get("/posts", ctr.getPosts);

router.post("/post", ctr.newPost);

router.delete("/delete/:id", ctr.deletePost);

export { router };
