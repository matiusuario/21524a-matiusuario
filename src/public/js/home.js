import { fetchPosts, borrarPost } from "./funciones.js";

window.addEventListener("DOMContentLoaded", async () => {
    
    const posts = await fetchPosts();
    const publis = document.querySelector("#publis");
    posts.forEach(p => {
        let div = document.createElement("div");
        let divId = p.id.toString();
        //div id no se está guardando adecuadamente.
        div.innerHTML = 
            `<div id="${divId}" class="column my-3 pt-3">
                <div class="px-2 py-3 pub">
                    <h2 class="tit mb-3">${p.titulo}</h2>
                    <p class=fechaCr">${p.fecha_creacion.slice(0, 10)}</p>
                    <div class="col col-md-8 col-lg-6 mb-5">
                        <img class="img-fluid imgurl" src="${p.url_imagen}" alt="Imagen de la publicacion ${p.titulo}">
                    </div>
                    <p class="cont mb-4">${p.contenido}</p>
                    <button type="button" value="${p.id}" class="btn btn-sm btn-outline-secondary btn-editar">Editar publicación</button>
                    <button value="${p.id}" class="btn btn-sm btn-outline-secondary btn-borrar">Borrar publicación</button>
                </div>
            </div>`;
        publis.appendChild(div);
    });
});

window.addEventListener("load", () => {
    //No es lo optimo pero utiliza un timeout para darle tiempo a que carguen las publicaciones
    setTimeout(() => {
        const delBtns = document.querySelectorAll(".btn-borrar");
        const editBtns = document.querySelectorAll(".btn-editar");
        delBtns.forEach((btn) => {
            btn.addEventListener("click", async () => {
                if (confirm("¿Seguro que desea ELIMINAR la publicación?")) {
                    const resp = await borrarPost(btn.value);
                    alert(resp.msg);
                    window.location.reload();
                }
            });
        });
        editBtns.forEach((btn) => {
            btn.addEventListener("click", async () => {
                if (confirm("¿Seguro que desea modificar la publicación?")) {
                    window.location.href = `/edit/${btn.value}`;
                }
            });
        });
    }, 1000);
});
