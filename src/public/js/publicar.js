import { postearPost } from "./funciones.js";

const cancelar = document.querySelector("#cancelar");
const form = document.querySelector("#publicarForm");

cancelar.addEventListener("click", () => history.back());

form.addEventListener("submit", async (event) => {
    
    event.preventDefault();
    const post = {
        titulo: document.querySelector("#titulo").value,
        contenido: document.querySelector("#contenido").value,
        url_imagen: document.querySelector("#url").value,
        fecha_creacion: document.querySelector("#fecha").value
    }
    const resp = await postearPost(post);
    alert(resp.msg);
    window.location.href = "/";
});
