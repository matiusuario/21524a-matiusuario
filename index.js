import express from "express";
import cors from "cors";
import morgan from "morgan";
import ejs from "ejs";
import 'dotenv/config';
import { router } from "./src/routes/routes.js";
import { conectarDb, sequelize } from "./src/database.js";

const PORT = process.env.PORT || 3000;
const app = express();

app.set("view engine", "ejs");
app.set("views", "./src/views");

conectarDb();

app.use(cors());
app.use(express.json());
app.use(router);
app.use(express.static("./src/public"));

app.listen(PORT, () => console.log(`Servidor corriendo en http://localhost:${PORT}`));
